package net.iescierva.mim.a201quicklauncher;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Attempts to launch an actrivity within our own app
        Button secondActivityButton=findViewById(R.id.secondActivityButton);
        secondActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent=new Intent(getApplicationContext(),SecondActivity.class);
                //shows how to pass information to another activity
                startIntent.putExtra("net.iescierva.mim.quicklauncher.SOMETHING","Hello, World!");  //key-value

                startActivity(startIntent);
            }
        });

        //Attempts to launch an actrivity outside our own app
        Button googleButton=findViewById(R.id.googleButton);
        googleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String google="https://maps.google.com";
                Uri webaddress=Uri.parse(google);

                Intent gotoGoogle=new Intent(Intent.ACTION_VIEW,webaddress);
                if (gotoGoogle.resolveActivity(getPackageManager()) != null) {
                    startActivity(gotoGoogle);
                }
            }
        });
    }
}
